<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'EntryController@get_list');

Route::post('store', 'EntryController@store');

Route::get('/store', function(){
    return view('forms.store');
});

Route::get('/delete/{id}', 'EntryController@delete');

Route::post('/update', 'EntryController@update');

Route::get('/download/{id}', 'EntryController@download');