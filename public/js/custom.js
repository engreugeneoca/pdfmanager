$('#preview').on('show.bs.modal', function () {
  var drawOrPayValue = $('#drawOrPay option:selected').text();
  var moreThreeYearsBusinessValue = $('#moreThreeYearsBusiness option:selected').text();
  var businessYearsValue = $('#businessYears').val();
  var fulltimeValue = $('#fulltime option:selected').text();
  var companySizeValue = $('#companySize').val();
  var toolsValue = $('#tools').val();
  var hasOtherShareholderValue = $('#hasOtherShareholder').val();
  var annualProfitValue = $('#annualProfit').val();
  var paymentFrequencyValue = $('#paymentFrequency option:selected').text();
  var accPlanValue = $('#accPlan option:selected').text();
  var nominatedCoverPlusValue = $('#nominatedCoverPlus').val();

  $("#ansDrawOrPay").text(drawOrPayValue);
  $("#ansMoreThreeYearsBusiness").text(moreThreeYearsBusinessValue);
  $("#ansBusinessYears").text(businessYearsValue);
  $("#ansFulltime").text(fulltimeValue);
  $("#ansCompanySize").text(companySizeValue);
  $("#ansTools").text(toolsValue);
  $("#ansHasOtherShareholder").text(hasOtherShareholderValue);
  $("#ansAnnualProfit").text(annualProfitValue);
  $("#ansPaymentFrequency").text(paymentFrequencyValue);
  $("#ansAccPlan").text(accPlanValue);
  $("#ansNominatedCoverPlus").text(nominatedCoverPlusValue);
});

$('#btn-download').click(function(event){
  $('#fillform').submit();
});

$("[id^=btn-edit-]").click(function(event){
  event.preventDefault();
  $("#btn-save").removeAttr("disabled");
  $("#drawOrPay").val('');
  $("#moreThreeYearsBusiness").val('');
  $("#businessYears").val('');
  $("#fulltime").val('');
  $("#companySize").val('');
  $("#tools").val('');
  $("#hasOtherShareholder").val('');
  $("#annualProfit").val('');
  $("#paymentFrequency").val('');
  $("#accPlan").val('');
  $("#nominatedCoverPlus").val('');
});

$('[id^=btn-edit-]').click(function(){
  var row_id = this.id.split('-')[2];
  var drawOrPay = $("#drawOrPay-"+row_id).text();
  var moreThreeYearsBusiness = $("#moreThreeYearsBusiness-"+row_id).text();
  var businessYears = $("#businessYears-"+row_id).text();
  var fulltime = $("#fulltime-"+row_id).text();
  var companySize = $("#companySize-"+row_id).text();
  var tools = $("#tools-"+row_id).text();
  var hasOtherShareholder = $("#hasOtherShareholder-"+row_id).text();
  var annualProfit = $("#annualProfit-"+row_id).text();
  var paymentFrequency = $("#paymentFrequency-"+row_id).text();
  var accPlan = $("#accPlan-"+row_id).text();
  var nominatedCoverPlus = $("#nominatedCoverPlus-"+row_id).text();

  $('#accountid').val(row_id);
  $('#drawOrPay').val(drawOrPay);
  $('#moreThreeYearsBusiness').val(moreThreeYearsBusiness);
  $('#businessYears').val(businessYears);
  $('#fulltime').val(fulltime);
  $('#companySize').val(companySize);
  $('#tools').val(tools);
  $('#hasOtherShareholder').val(hasOtherShareholder);
  $('#annualProfit').val(annualProfit);
  $('#paymentFrequency').val(paymentFrequency);
  $('#accPlan').val(accPlan);
  $('#nominatedCoverPlus').val(nominatedCoverPlus);
});