<?php

namespace App\Http\Controllers;

use App\Entry;
use Illuminate\Http\Request;
use PDF;

class EntryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function get_list(){
        $entry = Entry::paginate(5);
        return view('dashboard')->with(['entries'=>$entry]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $criteria = [
            'drawOrPay'  =>  'required',
            'moreThreeYearsBusiness'  =>  'required',
            'businessYears'  =>  'required',
            'fulltime'  =>  'required',
            'companySize'  =>  'required',
            'tools'  =>  'required',
            'hasOtherShareholder'  =>  'required',
            'annualProfit'  =>  'required',
            'paymentFrequency'  =>  'required',
            'accPlan'  =>  'required',
            'nominatedCoverPlus'  =>  'required',
        ];

        $data = [
            'drawOrPay' => $request->drawOrPay,
            'moreThreeYearsBusiness' => $request->moreThreeYearsBusiness,
            'businessYears' => $request->businessYears,
            'fulltime' => $request->fulltime,
            'companySize' => $request->companySize,
            'tools' => $request->tools,
            'hasOtherShareholder' => $request->hasOtherShareholder,
            'annualProfit' => $request->annualProfit,
            'paymentFrequency' => $request->paymentFrequency,
            'accPlan' => $request->accPlan,
            'nominatedCoverPlus' => $request->nominatedCoverPlus,
        ];

        $request->validate($criteria);
        $entry = new Entry();
        $entry->drawOrPay = $request->drawOrPay;
        $entry->moreThreeYearsBusiness = $request->moreThreeYearsBusiness;
        $entry->businessYears = $request->businessYears;
        $entry->fulltime = $request->fulltime;
        $entry->companySize = $request->companySize;
        $entry->tools = $request->tools;
        $entry->hasOtherShareholder = $request->hasOtherShareholder;
        $entry->annualProfit = $request->annualProfit;
        $entry->paymentFrequency = $request->paymentFrequency;
        $entry->accPlan = $request->accPlan;
        $entry->nominatedCoverPlus = $request->nominatedCoverPlus;
        $entry->save();
        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entry  $entry
     * @return \Illuminate\Http\Response
     */
    public function show(Entry $entry)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entry  $entry
     * @return \Illuminate\Http\Response
     */
    public function edit(Entry $entry)
    {
        //
    }

    public function update(Request $request)
    {
        $entry = Entry::where('id',$request->accountid)->first();
        $entry->drawOrPay = $request->drawOrPay;
        $entry->moreThreeYearsBusiness = $request->moreThreeYearsBusiness;
        $entry->businessYears = $request->businessYears;
        $entry->fulltime = $request->fulltime;
        $entry->companySize = $request->companySize;
        $entry->tools = $request->tools;
        $entry->hasOtherShareholder = $request->hasOtherShareholder;
        $entry->annualProfit = $request->annualProfit;
        $entry->paymentFrequency = $request->paymentFrequency;
        $entry->accPlan = $request->accPlan;
        $entry->nominatedCoverPlus = $request->nominatedCoverPlus;
        $entry->update();
        return redirect('/');
    }

    public function download($id){
        $entry = Entry::where('id',$id)->first();

        $data = [
            'drawOrPay' => $entry->drawOrPay,
            'moreThreeYearsBusiness' => $entry->moreThreeYearsBusiness,
            'businessYears' => $entry->businessYears,
            'fulltime' => $entry->fulltime,
            'companySize' => $entry->companySize,
            'tools' => $entry->tools,
            'hasOtherShareholder' => $entry->hasOtherShareholder,
            'annualProfit' => $entry->annualProfit,
            'paymentFrequency' => $entry->paymentFrequency,
            'accPlan' => $entry->accPlan,
            'nominatedCoverPlus' => $entry->nominatedCoverPlus,
        ];
        $pdf = PDF::loadView('templates.formtemplate', $data);
        return $pdf->download('document.pdf');
    }

    public function delete($id)
    {
        $entry = Entry::where('id',$id)->delete();
        return redirect('/');
    }
}
