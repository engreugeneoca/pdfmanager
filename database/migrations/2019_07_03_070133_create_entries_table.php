<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('drawOrPay');
            $table->string('moreThreeYearsBusiness');
            $table->string('businessYears');
            $table->string('fulltime');
            $table->string('companySize');
            $table->string('tools');
            $table->string('hasOtherShareholder');
            $table->string('annualProfit');
            $table->string('paymentFrequency');
            $table->string('accPlan');
            $table->string('nominatedCoverPlus');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entries');
    }
}
