<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>PDF Remaker</title>

    <!-- Bootstrap core CSS -->
    <link href="{{url('/')}}/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


    <body class="bg-light">
    <div class="container">
  <div class="py-5 text-center">
    <h2>PDF Manager</h2>
    </div>

  <div class="row">
    <div class="col-md-12 order-md-1">
        <h4 class="mb-3">Please fill-up this form</h4>
        <form action="{{url('/store')}}" method="POST" id="fillform">
        @csrf
        @if (count($errors) >0)
            <div class="alert alert-danger">
                <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach  
                </ul>
            </div>  
        @endif 
        <div class="row">
            <label for="drawOrPay">Are you taking Drawings or Paye?</label>
            <select class="custom-select d-block w-100" id="drawOrPay" name="drawOrPay" required>
              <option value="">Choose...</option>
              <option>PAYE</option>
              <option>Drawings/Shareholder salary (i.e non paye income)</option>
              <option>Combination of Drawings/Shareholder salary (i.e non paye income) and PAYE</option>
            </select>
        </div>
        <div class="row">
            <label for="moreThreeYearsBusiness">Been in business in more than 3 years?</label>
            <select class="custom-select d-block w-100" id="moreThreeYearsBusiness" name="moreThreeYearsBusiness" required>
              <option value="">Choose...</option>
              <option>Yes</option>
              <option>No</option>
            </select>
        </div>
        <div class="row">
            <label for="businessYears">How many years?</label>
            <input type="text" class="form-control" id="businessYears" name="businessYears" placeholder="" value="" required>
        </div>
        <div class="row">
            <label for="fulltime">Working Full-time?</label>
            <select class="custom-select d-block w-100" id="fulltime" name="fulltime" required>
              <option value="">Choose...</option>
              <option>Part Time</option>
              <option>Full Time</option>
            </select>
        </div>
        <div class="row">
            <label for="companySize">How many staff do you have working for you?</label>
            <input type="text" class="form-control" id="companySize" name="companySize" placeholder="" value="" required>
        </div>
        <div class="row">
            <label for="tools">Now, are you on tools?</label>
            <input type="text" class="form-control" id="tools" name="tools" placeholder="" value="" required>
        </div>
        <div class="row">
            <label for="hasOtherShareholder">Is there any other shareholders/director that are not on the tools?</label>
            <input type="text" class="form-control" id="hasOtherShareholder" name="hasOtherShareholder" placeholder="" value="" required>
        </div>
        <div class="row">
            <label for="annualProfit">How much income did you take out last year from the business?</label>
            <input type="text" class="form-control" id="annualProfit" name="annualProfit" placeholder="" value="" required>
        </div>
        <div class="row">
            <label for="paymentFrequency">Payment Frequency</label>
            <select class="custom-select d-block w-100" id="paymentFrequency" name="paymentFrequency" required>
              <option value="">Choose...</option>
              <option>Weekly</option>
              <option>Fortnightly</option>
              <option>Monthly</option>
              <option>Annually</option>
            </select>
        </div>
        <div class="row">
            <label for="accPlan">What ACC cover plan do you have?</label>
            <select class="custom-select d-block w-100" id="accPlan" name="accPlan" required>
              <option value="">Choose...</option>
              <option>ACC Cover Plus/Work Place Cover</option>
              <option>ACC Cover Plus Extra</option>
            </select>
        </div>
        <div class="row">
            <label for="nominatedCoverPlus">Your nominated Cover PLus Extra cover amount</label>
            <input type="text" class="form-control" id="nominatedCoverPlus" name="nominatedCoverPlus" placeholder="" value="" required>
        </div>
    </div>
  </div>
  <hr class="mb-4">
  </form>
  <div class="row">
        <button class="col-md-4 offset-md-1 btn btn-primary btn-lg" data-toggle="modal" data-target="#preview">Preview PDF</button>
        <button class="col-md-4 offset-md-2 btn btn-primary btn-lg" type="submit" id="btn-download">Store & Download</button>
  </div>

    <!--Modal for preview-->

    <!-- Modal -->
    <div class="modal fade" id="preview" tabindex="-1" role="dialog" aria-labelledby="previewLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="previewLabel">Preview</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <b class="col-md-12">Are you taking Drawings or Paye?</b>
                <i class="col-md-12"><u> Answer: </u>
                    <div id="ansDrawOrPay"></div>
                </i>
            </div>
            <div class="row">
                <b class="col-md-12">Been in business in more than 3 years?</b>
                <i class="col-md-12"><u> Answer: </u>
                    <div id="ansMoreThreeYearsBusiness"></div>
                </i>
            </div>
            <div class="row">
                <b class="col-md-12">How many years?</b>
                <i class="col-md-12"><u> Answer: </u>
                    <div id="ansBusinessYears"></div>
                </i>
            </div>
            <div class="row">
                <b class="col-md-12">Working Full-time?</b>
                <i class="col-md-12"><u> Answer: </u>
                    <div id="ansFulltime"></div>
                </i>
            </div>
            <div class="row">
                <b class="col-md-12">How many staff do you have working for you?</b>
                <i class="col-md-12"><u> Answer: </u>
                    <div id="ansCompanySize"></div>
                </i>
            </div>
            <div class="row">
                <b class="col-md-12">Now, are you on tools?</b>
                <i class="col-md-12"><u> Answer: </u>
                    <div id="ansTools"></div>
                </i>
            </div>
            <div class="row">
                <b class="col-md-12">Is there any other shareholders/director that are not on the tools?</b>
                <i class="col-md-12"><u> Answer: </u>
                    <div id="ansHasOtherShareholder"></div>
                </i>
            </div>
            <div class="row">
                <b class="col-md-12">How much income did you take out last year from the business?</b>
                <i class="col-md-12"><u> Answer: </u>
                    <div id="ansAnnualProfit"></div>
                </i>
            </div>
            <div class="row">
                <b class="col-md-12">Payment Frequency</b>
                <i class="col-md-12"><u> Answer: </u>
                    <div id="ansPaymentFrequency"></div>
                </i>
            </div>
            <div class="row">
                <b class="col-md-12">What ACC cover plan do you have?</b>
                <i class="col-md-12"><u> Answer: </u>
                    <div id="ansAccPlan"></div>
                </i>
            </div>
            <div class="row">
                <b class="col-md-12">Your nominated Cover PLus Extra cover amount</b>
                <i class="col-md-12"><u> Answer: </u>
                    <div id="ansNominatedCoverPlus"></div>
                </i>
            </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
    </div>

  <footer class="my-5 pt-5 text-muted text-center text-small">
    <p class="mb-1">&copy; 2017-2019 JD Life</p>
    <ul class="list-inline">
      <li class="list-inline-item"><a href="#">Privacy</a></li>
      <li class="list-inline-item"><a href="#">Terms</a></li>
      <li class="list-inline-item"><a href="#">Support</a></li>
    </ul>
  </footer>
  
</div>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="{{url('/')}}/js/bootstrap.bundle.min.js" integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o" crossorigin="anonymous"></script>
<script src="{{url('/')}}/js/custom.js" crossorigin="anonymous"></script>
</body>