<!doctype html>
<html lang="en">
<body class="bg-light">
    <div class="container">
    <div class="row">
        <div class="col-md-12 order-md-1">
            <h4 class="mb-3">Your response</h4>
            <div class="row">
                <b>Are you taking Drawings or Paye?</b>
                <i><u> Answer:</u>{{$drawOrPay}}</i>
            </div>
            <div class="row">
                <b>Been in business in more than 3 years?</b>
                <i><u> Answer: </u>{{$moreThreeYearsBusiness}}</i>
            </div>
            <div class="row">
                <b>How many years?</b>
                <i><u> Answer: </u>{{$businessYears}}</i>
            </div>
            <div class="row">
                <b>Working Full-time?</b>
                <i><u> Answer: </u>{{$fulltime}}</i>
            </div>
            <div class="row">
                <b>How many staff do you have working for you?</b>
                <i><u> Answer: </u>{{$companySize}}</i>
            </div>
            <div class="row">
                <b>Now, are you on tools?</b>
                <i><u> Answer: </u>{{$tools}}</i>
            </div>
            <div class="row">
                <b>Is there any other shareholders/director that are not on the tools?</b>
                <i><u> Answer: </u>{{$hasOtherShareholder}}</i>
            </div>
            <div class="row">
                <b>How much income did you take out last year from the business?</b>
                <i><u> Answer: </u>{{$annualProfit}}</i>
            </div>
            <div class="row">
                <b>Payment Frequency</b>
                <i><u> Answer: </u>{{$paymentFrequency}}</i>
            </div>
            <div class="row">
                <b>What ACC cover plan do you have?</b>
                <i><u> Answer: </u>{{$accPlan}}</i>
            </div>
            <div class="row">
                <b>Your nominated Cover PLus Extra cover amount</b>
                <i><u> Answer: </u>{{$nominatedCoverPlus}}</i>
            </div>
        </div>
  </div>
</body>
</html>