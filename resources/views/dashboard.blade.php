<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>PDF Manager</title>

    <!-- Bootstrap core CSS -->
    <link href="{{url('/')}}/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <div class="py-5 text-center">
    <h2>PDF Manager</h2>
    </div>



<div class="p-3">
    <div class="table-responsive">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Draw or PAYE</th>
                <th scope="col"> >3 Years</th>
                <th scope="col">Years in Business</th>
                <th scope="col">Fulltime/Part-time</th>
                <th scope="col">Company Size</th>
                <th scope="col">Tools</th>
                <th scope="col">Other Shareholders</th>
                <th scope="col">Annual Profit</th>
                <th scope="col">Payment Frequency</th>
                <th scope="col">ACC Plan</th>
                <th scope="col">Nominated Cover Plus</th>
                <th scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach($entries as $key => $row)
                <tr>
                <th scope="row">{{$entries->firstItem()+$key}}</th>
                    <td id="drawOrPay-{{$row['id']}}">{{$row['drawOrPay']}}</td>
                    <td id="moreThreeYearsBusiness-{{$row['id']}}">{{$row['moreThreeYearsBusiness']}}</td>
                    <td id="businessYears-{{$row['id']}}">{{$row['businessYears']}}</td>
                    <td id="fulltime-{{$row['id']}}">{{$row['fulltime']}}</td>
                    <td id="companySize-{{$row['id']}}">{{$row['companySize']}}</td>
                    <td id="tools-{{$row['id']}}">{{$row['tools']}}</td>
                    <td id="hasOtherShareholder-{{$row['id']}}">{{$row['hasOtherShareholder']}}</td>
                    <td id="annualProfit-{{$row['id']}}">{{$row['annualProfit']}}</td>
                    <td id="paymentFrequency-{{$row['id']}}">{{$row['paymentFrequency']}}</td>
                    <td id="accPlan-{{$row['id']}}">{{$row['accPlan']}}</td>
                    <td id="nominatedCoverPlus-{{$row['id']}}">{{$row['nominatedCoverPlus']}}</td>
                    <td>
                    <a class="btn btn-primary btn-sm" href="{{url('/download').'/'.$row['id']}}">Download</a>
                    <button class="btn btn-success btn-sm" data-toggle="modal" data-target="#edituser-modal" id="btn-edit-{{$row['id']}}">Edit</button>
                    <a class="btn btn-danger btn-sm" href="{{url('/delete').'/'.$row['id']}}">Delete</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table><!-- ./table --> 
    </div><!-- ./table-responsive -->

    <div class="d-flex justify-content-between align-items-center">
            <a class="btn btn-success btn-sm" href="{{url('/store')}}">Add Response</a>
            <!-- pagination -->
            {{$entries->links()}}
            <!-- ./pagination -->
    </div>
</div> <!-- ./p-3 -->


<!--  edit-modal  -->
<div class="modal fade" id="edituser-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Edit User</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="p-3">    
                <form method="POST" action="{{url('/update')}}" id="editform">
                    @csrf
                    <input type="hidden" id="accountid" name="accountid" value="">
                    @if (count($errors) >0)
                        <div class="alert alert-danger">
                            <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach  
                            </ul>
                        </div>  
                    @endif 
                    <div class="row">
                        <label for="drawOrPay">Are you taking Drawings or Paye?</label>
                        <select class="custom-select d-block w-100" id="drawOrPay" name="drawOrPay" required>
                        <option value="">Choose...</option>
                        <option>PAYE</option>
                        <option>Drawings/Shareholder salary (i.e non paye income)</option>
                        <option>Combination of Drawings/Shareholder salary (i.e non paye income) and PAYE</option>
                        </select>
                    </div>
                    <div class="row">
                        <label for="moreThreeYearsBusiness">Been in business in more than 3 years?</label>
                        <select class="custom-select d-block w-100" id="moreThreeYearsBusiness" name="moreThreeYearsBusiness" required>
                        <option value="">Choose...</option>
                        <option>Yes</option>
                        <option>No</option>
                        </select>
                    </div>
                    <div class="row">
                        <label for="businessYears">How many years?</label>
                        <input type="text" class="form-control" id="businessYears" name="businessYears" placeholder="" value="" required>
                    </div>
                    <div class="row">
                        <label for="fulltime">Working Full-time?</label>
                        <select class="custom-select d-block w-100" id="fulltime" name="fulltime" required>
                        <option value="">Choose...</option>
                        <option>Part Time</option>
                        <option>Full Time</option>
                        </select>
                    </div>
                    <div class="row">
                        <label for="companySize">How many staff do you have working for you?</label>
                        <input type="text" class="form-control" id="companySize" name="companySize" placeholder="" value="" required>
                    </div>
                    <div class="row">
                        <label for="tools">Now, are you on tools?</label>
                        <input type="text" class="form-control" id="tools" name="tools" placeholder="" value="" required>
                    </div>
                    <div class="row">
                        <label for="hasOtherShareholder">Is there any other shareholders/director that are not on the tools?</label>
                        <input type="text" class="form-control" id="hasOtherShareholder" name="hasOtherShareholder" placeholder="" value="" required>
                    </div>
                    <div class="row">
                        <label for="annualProfit">How much income did you take out last year from the business?</label>
                        <input type="text" class="form-control" id="annualProfit" name="annualProfit" placeholder="" value="" required>
                    </div>
                    <div class="row">
                        <label for="paymentFrequency">Payment Frequency</label>
                        <select class="custom-select d-block w-100" id="paymentFrequency" name="paymentFrequency" required>
                        <option value="">Choose...</option>
                        <option>Weekly</option>
                        <option>Fortnightly</option>
                        <option>Monthly</option>
                        <option>Annually</option>
                        </select>
                    </div>
                    <div class="row">
                        <label for="accPlan">What ACC cover plan do you have?</label>
                        <select class="custom-select d-block w-100" id="accPlan" name="accPlan" required>
                        <option value="">Choose...</option>
                        <option>ACC Cover Plus/Work Place Cover</option>
                        <option>ACC Cover Plus Extra</option>
                        </select>
                    </div>
                    <div class="row">
                        <label for="nominatedCoverPlus">Your nominated Cover PLus Extra cover amount</label>
                        <input type="text" class="form-control" id="nominatedCoverPlus" name="nominatedCoverPlus" placeholder="" value="" required>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" id="btn-save" disabled>SAVE CHANGES</button>
                    </div>
                </form><!-- ./form -->        
            </div>
        </div>
    </div>
    </div>
    <!-- ./edit-modal -->


  
</div>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="{{url('/')}}/js/bootstrap.bundle.min.js" integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o" crossorigin="anonymous"></script>
<script src="{{url('/')}}/js/custom.js" crossorigin="anonymous"></script>
</body>